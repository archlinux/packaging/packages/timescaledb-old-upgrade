# Maintainer: George Rawlinson <george@rawlinson.net.nz>

_pkgname=timescaledb
pkgname=timescaledb-old-upgrade
_current_pg_version=17
_old_pg_version=$(( _current_pg_version - 1 ))
# limit to last 5 releases
declare -gA _commits=(
  [2.17.0]=43ef9b7bda813c10f845fe55f7fc6113bbd47b07
  [2.17.1]=beb03d6d4c61dbd02291ef2c661c00155ddb79c9
  [2.17.2]=b359d26de186ea43f93c28c08cd1b8c6449c91bd
  [2.18.0]=91700b509acb0a4511b5028b902d4b3ed0d04447
  [2.18.1]=0660ebc3fcf4e6859159ba42aeced56333d02021
  [2.18.2]=bb49cfc6d626782a214b6b6e343a0666d7c40773
)
pkgver=2.18.2
pkgrel=1
pkgdesc='TimescaleDB build for migrating between major versions of PostgreSQL'
arch=('x86_64')
url='https://www.timescale.com/'
license=('Apache-2.0' 'LicenseRef-Timescale')
depends=(
  "postgresql-old-upgrade<$_current_pg_version"
  "postgresql-old-upgrade>=$_old_pg_version"
  "timescaledb=$pkgver"
)
makedepends=('gcc' 'cmake' 'git')
source=("$_pkgname::git+https://github.com/timescale/timescaledb#commit=${_commits[$pkgver]}")
sha512sums=('027aee2c0a0841e9da7ef4c42c47a9e3d8e2b0816a87881747de6d9a17596bd6109428f0c2b5ab0a5fdacc0efdcfe66b39308571dd6f1558f5a0b1e166dd7314')
b2sums=('47320c71f35f3b6a681fd39e6127f80b1840942d52a4a465b8fb7a057c85ad8b13ca94841c375bbed0b30d6168d959f92b7dda01090ed669ef0a3f482ca2d696')

prepare() {
  mkdir -p build
}

build() {
  local version
  for version in "${!_commits[@]}"; do
    cd "$srcdir/$_pkgname"

    git checkout "${_commits[$version]}"

    BUILD_DIR="$srcdir/build/$version" ./bootstrap \
      -DWARNINGS_AS_ERRORS=OFF \
      -DREGRESS_CHECKS=OFF \
      -DPG_PATH="/opt/pgsql-${_old_pg_version}"

    # build package or past shared library
    cd "$srcdir/build/$version"

    # ensure reproducible builds (value from `lsb_release -r`)
    # TODO: resolve https://github.com/timescale/timescaledb/issues/3480
    sed \
      -e "s:BUILD_OS_VERSION \".*\"$:BUILD_OS_VERSION \"rolling\":" \
      -i src/config.h

    if [[ $version == $pkgver ]]; then
      make
    else
      make timescaledb timescaledb-tsl sqlfile
    fi
  done
}

package() {
  cd "$_pkgname"
  # install licenses from latest version
  git checkout "${_commits[$pkgver]}"
  install -vDm644 -t "$pkgdir/usr/share/licenses/$pkgname" \
    LICENSE LICENSE-APACHE tsl/LICENSE-TIMESCALE

  # install package or past shared library
  local version
  for version in "${!_commits[@]}"; do
    cd "$srcdir/build/$version"
    if [[ $version == $pkgver ]]; then
      make DESTDIR="$pkgdir/" install
    else
      install -vDm755 -t "$pkgdir/opt/pgsql-${_old_pg_version}/lib" \
        "src/$_pkgname-$version.so" \
        "tsl/src/$_pkgname-tsl-$version.so"
      install -Dm644 -t "$pkgdir/opt/pgsql-${_old_pg_version}/share/extension" \
        "sql/timescaledb--$version.sql"
    fi
  done
}
